# lb4-app

[![LoopBack](https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png)](http://loopback.io/)


### Models
1. User
2. Role
3. UserRole

### Relations
1. UserRole ```@belongsTo()``` Role, User

### Authorization 

| endpoints  |user   | admin  | contentmanager  |  no-login   |
|---|---|---|---|---|
|  /ping   |OK   | OK  | OK  | OK  |
| /users/login    |  OK |OK   |  OK |OK   |
|  /ping/permit-all   | OK  |  OK |OK   | OK  |
|/ping/deny-all    |401   |401   | 401  |  401 |
|  /ping/is-authenticated   |  OK | OK  | OK  | 401  |
| /ping/has-any-role   | 401  | OK  | OK  | 401  |
| /ping/has-roles    | 401  | 401  |OK   | 401  |

### Usage

```
/users/login  

username:admin
password:password

In response token will be generated, copy the token

Example: "http://localhost:3000/ping/has-any-role?access_token=<paste_token>"

```